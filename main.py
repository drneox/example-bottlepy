from bottle import run
from beaker.middleware import SessionMiddleware
from app.controller import app

if __name__ == '__main__':
    session_opts = {
        'session.type': 'cookie',
        'session.validate_key': True,
        }

    app = SessionMiddleware(app, session_opts)
    run(app=app, reloader=True)