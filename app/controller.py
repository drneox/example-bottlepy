from bottle import Bottle, request, template, redirect
from app.model import auth

app = Bottle()

@app.route('/')
def home():
    return template('home')

@app.route('/item/<id:int>')
def item(id):
    items = {1: {'title': 'title  number one', 'body': 'item number one'},
            2: {'title': 'title  number two', 'body': 'item number two'},
            3: {'title': 'title  number three', 'body': 'item number three'},
            4: {'title': 'title  number four', 'body': 'item number four'}}
    if id:
        item= items[id]
    return template('item', item)

@app.route('/login', method=['GET', 'POST'])
def login():
    username = request.POST.get('username', )
    password = request.POST.get('password', )
    if username and password:
        auth.login(username, password, success_redirect='/admin', fail_redirect='/login')
    return template('login')

@app.route('/logout')
def logout():
    auth.logout(success_redirect='/login')

@app.route('/admin')
def admin():
    auth.require(fail_redirect='/login')
    return "welcome %s :)" % auth.current_user.username
