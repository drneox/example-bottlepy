from cork.backends import MongoDBBackend
from cork import Cork


def mongo_backend():
    db = MongoDBBackend(db_name='bottle', initialize=True)
    #create user and role admin
    # db.users._coll.insert({
    #     "login": "admin",
    #     "email_addr": "admin@localhost.local",
    #     "desc": "admin test user",
    #     "role": "admin",
    #     "hash": "cLzRnzbEwehP6ZzTREh3A4MXJyNo+TV8Hs4//EEbPbiDoo+dmNg22f2RJC282aSwgyWv/O6s3h42qrA6iHx8yfw=",
    #     "creation_date": "2012-10-28 20:50:26.286723"
    # })
    # db.roles._coll.insert({'role': 'admin', 'val': 100})
    return db
db = mongo_backend()

auth = Cork(backend=db)